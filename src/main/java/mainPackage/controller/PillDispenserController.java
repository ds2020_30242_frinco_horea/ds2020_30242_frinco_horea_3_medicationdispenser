package mainPackage.controller;

import mainPackage.model.PillDispenserModel;
import mainPackage.services.PillDispenserService;
import mainPackage.view.PillDispenserView;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.UUID;

public class PillDispenserController {
    private PillDispenserModel model;
    private PillDispenserView view;
    private PillDispenserService service;
    private UUID patientId = UUID.fromString("7220d482-5b5b-4fe7-ac75-dd49ad75e730");

    public PillDispenserController(PillDispenserModel model, PillDispenserView view, PillDispenserService service) {
        this.model = model;
        this.view = view;
        this.service = service;

        model.setCurrentTime(new Date());
        model.setPatientWelcome(service.sayHelloWithHessian(patientId));

        view.setWelcomeLabelText();
        view.setTimeLabelText();
        Object[][] data = service.getMedicationData(patientId);
        String[] columns = { "Medication name", "Interval", "Medication period" };
        model.setMedicationTableColumns(columns);
        model.setMedicationTableData(data);
        view.setMedicationTableData(new CustomAction());
        view.initTimeRefreshTimer(1000, new TimeRefreshTimerListener());
        view.initTableRefreshTimer(3600*1000, new TableRefreshTimerListener());
    }

    class TimeRefreshTimerListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            model.setCurrentTime(new Date());
            view.setTimeLabelText();
        }
    }

    class TableRefreshTimerListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            Object[][] data = service.getMedicationData(patientId);
            String[] columns = { "Medication name", "Interval", "Medication period" };
            model.setMedicationTableColumns(columns);
            model.setMedicationTableData(data);
            view.setMedicationTableData(new CustomAction());
        }
    }

    class CustomAction extends AbstractAction {
        @Override
        public void actionPerformed(ActionEvent e) {
            Object[][] medicationData = model.getMedicationTableData();
            Object[] row = medicationData[Integer.parseInt(e.getActionCommand())];

            Object[] dataPackage = new Object[5];
            dataPackage[0] = patientId;
            dataPackage[1] = row[0];
            dataPackage[2] = row[1];
            dataPackage[3] = row[2];

            LocalDateTime dateTime = LocalDateTime.now();
            String dateString =  dateTime.getYear() + "-" + dateTime.getMonthValue() + "-" + dateTime.getDayOfMonth() + " " + dateTime.getHour() + ":" + dateTime.getMinute() + ":" + dateTime.getSecond();
            dataPackage[4] = dateString;

            service.takeMedication(dataPackage);
            Object[][] newMedicationTableData = new Object[model.getMedicationTableData().length - 1][];
            int i = 0;
            int j = 0;
            for (Object[] tableRow : model.getMedicationTableData()) {
                if (Integer.parseInt(e.getActionCommand()) == j) {
                    j++;
                    continue;
                }
                newMedicationTableData[i] = tableRow;
                i++;
                j++;
            }
            model.setMedicationTableData(newMedicationTableData);
            view.setMedicationTableData(new CustomAction());
        }
    }
}
