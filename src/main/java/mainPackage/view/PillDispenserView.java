package mainPackage.view;

import mainPackage.model.PillDispenserModel;
import mainPackage.toolBox.ButtonColumn;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionListener;

public class PillDispenserView extends JFrame {
    private static JLabel   welcomeLabel    = new JLabel("Welcome ");
    private static JLabel   timeLabel       = new JLabel("Current time: ");
    private static JTable   medicationTable = new JTable();
    private static Timer    timeRefreshTimer;
    private static Timer    tableRefreshTimer;

    private PillDispenserModel model;

    public PillDispenserView(PillDispenserModel model) {
        this.model = model;

        JPanel p1 = new JPanel();
        p1.add(timeLabel);
        p1.add(welcomeLabel);
        p1.add(medicationTable.getTableHeader());
        p1.add(medicationTable);

        JPanel p2 = new JPanel();

        p1.setLayout(new BoxLayout(p1, BoxLayout.Y_AXIS));
        p2.setLayout(new GridLayout(0, 1));

        JPanel mainPanel = new JPanel();
        mainPanel.add(p1);
        mainPanel.add(p2);

        mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));

        this.setContentPane(mainPanel);
        this.pack();
        this.setTitle("Pill dispenser");
        this.setResizable(true);
        this.setSize(600,300);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    private void refreshFrame() {
        this.invalidate();
        this.validate();
        this.repaint();
    }

    public void setWelcomeLabelText() { welcomeLabel.setText(model.getPatientWelcome()); refreshFrame(); }

    public void setTimeLabelText() { timeLabel.setText(model.getCurrentTime()); refreshFrame(); }

    public void initTimeRefreshTimer(int delay, ActionListener listener) { timeRefreshTimer = new Timer(delay, listener); timeRefreshTimer.start(); }

    public void initTableRefreshTimer(int delay, ActionListener listener) { tableRefreshTimer = new Timer(delay, listener); tableRefreshTimer.start(); }

    public void setMedicationTableData(Action action) {
        medicationTable.removeAll();
        DefaultTableModel tableModel = (DefaultTableModel) medicationTable.getModel();
        tableModel.setDataVector(model.getMedicationTableData(), model.getMedicationTableColumns());
        medicationTable.setModel(tableModel);

        ButtonColumn buttonColumn = new ButtonColumn(medicationTable, action,medicationTable.getColumnCount() - 1);

        refreshFrame();
    }
}
