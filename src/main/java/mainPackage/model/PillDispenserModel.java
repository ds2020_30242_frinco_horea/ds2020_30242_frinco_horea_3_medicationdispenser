package mainPackage.model;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;

public class PillDispenserModel {
    private String[] medicationTableColumns;
    private Object[][] medicationTableData;
    private String currentTime;
    private String patientWelcome;

    public PillDispenserModel() {
        medicationTableColumns = null;
        medicationTableData = null;
        currentTime = "Current time: ";
        patientWelcome = "Welcome ";
    }

    public PillDispenserModel(String[] medicationTableColumns, Object[][] medicationTableData) {
        this.medicationTableColumns = medicationTableColumns;
        this.medicationTableData = medicationTableData;
        currentTime = "Current time: ";
        patientWelcome = "Welcome ";
    }

    public String[] getMedicationTableColumns() {
        return medicationTableColumns;
    }

    public void setMedicationTableColumns(String[] medicationTableColumns) {
        String[] columns = new String[medicationTableColumns.length + 1];
        int i = 0;
        for (String column : medicationTableColumns) {
            columns[i] = column;
            i++;
        }
        columns[i] = "Take medication";
        this.medicationTableColumns = columns;
    }

    public Object[][] getMedicationTableData() {
        return medicationTableData;
    }

    public void setMedicationTableData(Object[][] medicationTableData) {
        Object[][] data = new Object[medicationTableData.length][];
        int exclusions = 0;
        int i = 0;
        for (Object[] row : medicationTableData) {

            String[] treatmentPeriod = ((String)row[2]).split("-");
            DateFormat format = new SimpleDateFormat("yyyy/MM/dd", Locale.getDefault());
            Date endDate;
            Date startDate;
            Date currentDate;
            try {
                endDate = format.parse(treatmentPeriod[1]);
                startDate = format.parse(treatmentPeriod[0]);
                currentDate = new Date();

                System.out.println(startDate + "--->" + endDate);
                System.out.println("Current date: " + currentDate);
                if (endDate.before(currentDate)) {
                    System.out.println("Treatment done");
                    exclusions++;
                    continue;
                }
                if (startDate.after(currentDate)) {
                    System.out.println("Treatment not started");
                    exclusions++;
                    continue;
                }
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }

            data[i] = new Object[row.length + 1];
            int j = 0;
            for (Object obj : row) {
                data[i][j] = obj;
                j++;
            }
            data[i][j] = "Take medication";
            i++;
        }

        this.medicationTableData = Arrays.copyOfRange(data, 0, data.length - exclusions);
    }

    public String getCurrentTime() {
        return currentTime;
    }

    public void setCurrentTime(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        this.currentTime = "Current time: " + sdf.format(date);
    }

    public String getPatientWelcome() {
        return patientWelcome;
    }

    public void setPatientWelcome(String patientName) {
        this.patientWelcome = "Welcome " + patientName + "! Here is your medication for today: ";
    }
}
