package mainPackage;

import mainPackage.controller.PillDispenserController;
import mainPackage.model.PillDispenserModel;
import mainPackage.services.PillDispenserService;
import mainPackage.view.PillDispenserView;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.remoting.caucho.HessianProxyFactoryBean;

@SpringBootApplication
public class PillDispenser {

    @Bean
    public HessianProxyFactoryBean hessianInvoker() {
        HessianProxyFactoryBean invoker = new HessianProxyFactoryBean();
        invoker.setServiceUrl("http://medication-platform-horea.herokuapp.com/hellohessian");
        invoker.setServiceInterface(PillDispenserService.class);
        return invoker;
    }

    public static void main(String[] args) {
        SpringApplicationBuilder builder = new SpringApplicationBuilder(PillDispenser.class);
        builder.headless(false);
        ConfigurableApplicationContext context = builder.run(args);

        PillDispenserService service = context.getBean(PillDispenserService.class);
        PillDispenserModel model = new PillDispenserModel();
        PillDispenserView view = new PillDispenserView(model);
        PillDispenserController controller = new PillDispenserController(model, view, service);
        view.setVisible(true);
    }
}
