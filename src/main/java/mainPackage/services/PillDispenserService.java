package mainPackage.services;

import java.util.UUID;

public interface PillDispenserService {
    public String sayHelloWithHessian(UUID patientId);
    public Object[][] getMedicationData(UUID patientId);
    public void takeMedication(Object[] packageData);
}
